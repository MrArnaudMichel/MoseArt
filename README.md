# MoseArt

> [!NOTE]
> You can acces to MoseArt website **[here](https://moseart.arnaudmichel.fr)**!

Le projet MoseArt est un jeu de gestion au tour par tour mettant en vedette Robert Mose, directeur de l'IUT Robert Schuman d'Illkirch-Graffenstaden, pour un mandat de 5 ans. Développé par le groupe Meteor, le jeu vise à sensibiliser les joueurs aux responsabilités et défis liés à la gestion d'un établissement d'enseignement supérieur.

## Présentation du Projet

MoseArt offre une expérience immersive de gestion d'IUT, avec des objectifs pédagogiques clairs, des fonctionnalités de simulation détaillées et des scénarios variés pour tester les compétences des joueurs. À travers des captures d'écran attrayantes et des procédures d'installation simples, le jeu offre une expérience conviviale dès le départ.

## Objectifs Pédagogiques

Les objectifs pédagogiques de MoseArt incluent la compréhension du rôle du directeur, la gestion financière et administrative, la planification stratégique, le développement académique, la gestion du personnel, les relations publiques et la promotion de la recherche et de l'innovation. Des concepts avancés comme la massification de l'enseignement et la transition DUT-BUT sont également abordés.

## Fonctionnalités Principales

Le jeu propose une simulation réaliste de la gestion d'un IUT, avec des éléments clés à gérer tels que les ressources financières, la satisfaction et la réussite des étudiants et du personnel, ainsi que la gestion des événements aléatoires. L'interface intuitive permet aux joueurs de naviguer facilement et d'effectuer des actions stratégiques et opérationnelles pour influencer les performances de l'IUT.
